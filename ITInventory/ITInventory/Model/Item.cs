﻿using System.Drawing;

namespace ITInventory.Model
{
    /// <summary>
    /// Models an item in inventory.
    /// </summary>
    public class Item
    {
        public int ItemID { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public string DateAdded { get; set; }
        public int AddedByID { get; set; }
        public string AddedByUser { get; set; }
        public string LastModified { get; set; }
        public int ModifiedByID { get; set; }
        public string ModifiedByUser { get; set; }
        public string Location { get; set; }
        public Image Photo { get; set; }
    }
}
