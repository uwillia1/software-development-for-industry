﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITInventory.Model {

    /// <summary>
    /// Defaines the attributes of a user role
    /// </summary>
    public class Role
    {
        public int RoleID { get; set; }
        public string Name { get; set; }
    }
}
