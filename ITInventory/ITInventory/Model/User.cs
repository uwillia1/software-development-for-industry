﻿using ITInventory.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITInventory
{
    /// <summary>
    /// Defines the attributes of a user
    /// </summary>
    public class User
    {
        /// <summary>
        /// The User's unique ID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// User's unique username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// A randomly generated salt
        /// </summary
        [System.ComponentModel.Browsable(false)]
        public string PasswordSalt { get; set; }

        /// <summary>
        /// The user's hashed password to be stored in the database
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string PasswordHash { get; set; }

        /// <summary>
        /// User's first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User's last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// User's unique email address
        /// </summary>
        public string Email { get; set; }

        public Role Role { get; set; }

        public string RoleName { get; set; }

        public bool Enabled { get; set; }
    }
}
