﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ITInventory.Model
{
    /// <summary>
    /// Computes a hash for a given password
    /// </summary>
    public class PasswordHashser
    {
        private MD5CryptoServiceProvider hashAlgorithm;
        private const byte MIN_BYTE = 33;
        private const byte MAX_BYTE = 128;

        /// <summary>
        /// Makes a new instance of PasswordHasher
        /// </summary>
        public PasswordHashser()
        {
            this.hashAlgorithm = new MD5CryptoServiceProvider();
        }

        /// <summary>
        /// Computes the hash for the given salt and password strings
        /// </summary>
        /// <param name="salt">A randomly generated string</param>
        /// <param name="password">User's password</param>
        /// <returns></returns>
        public string ComputeHash(string salt, string password)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            //var saltBytes = Convert.FromBase64String(salt);
            //var passwordBytes = Convert.FromBase64String(password);
            var saltBytes = Encoding.UTF8.GetBytes(salt);
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] combinedBytes = new byte[saltBytes.Length + passwordBytes.Length];
            for (int i = 0; i < saltBytes.Length; i++)
            {
                combinedBytes[i] = saltBytes[i];
            }
            for (int i = 0; i < passwordBytes.Length; i++)
            {
                combinedBytes[saltBytes.Length + i] = passwordBytes[i];
            }
            var hashBytes = algorithm.ComputeHash(combinedBytes);
            for (var i = 0; i < hashBytes.Length; i++)
            {
                normalizeHashBytes(hashBytes, i);
            }
            return Encoding.UTF8.GetString(hashBytes);
        }

        private void normalizeHashBytes(byte[] hashBytes, int i)
        {
            if (hashBytes[i] > MAX_BYTE)
            {
                hashBytes[i] -= MAX_BYTE;
            }
            if (hashBytes[i] < MIN_BYTE)
            {
                hashBytes[i] += MIN_BYTE;
            }
        }
    }
}
