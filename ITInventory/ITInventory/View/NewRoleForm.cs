﻿using ITInventory.DAL;
using ITInventory.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory
{
    public partial class NewRoleForm : Form
    {
        private RoleRepository rolerepo;
        private MainPage parent;

        public NewRoleForm()
        {
            this.rolerepo = new RoleRepository();
            InitializeComponent();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            this.submitData();
        }

        private void submitData()
        {
            var roleExists = false;
            foreach (var role in this.rolerepo.ReadAll())
            {
                if (role.Name.ToUpper() == this.textBoxName.Text.ToUpper())
                {
                    roleExists = true;
                }
            }
            if (roleExists)
            {
                MessageBox.Show("Role already exists");
            }
            else
            {
                var newRole = new Role()
                {
                    Name = this.textBoxName.Text
                };
                this.rolerepo.Create(newRole);
                MessageBox.Show("New role added!");
                this.refreshAllRolesForm();
            }
            this.Close();
        }

        private void refreshAllRolesForm()
        {
            var openForms = this.parent.OpenForms;
            foreach (var form in openForms)
            {
                if (form.GetType() == typeof(AllRolesForm))
                {
                    var allRolesForm = form as AllRolesForm;
                    allRolesForm.RefreshDataGridView();
                }
            }
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewRoleForm_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void textBoxName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.submitData();
            }
        }

        private void NewRoleForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
        }

        private void NewRoleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }
    }
}
