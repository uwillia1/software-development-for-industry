﻿using ITInventory.DAL;
using ITInventory.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory.View
{
    public partial class AllItemsForm : Form
    {
        private ItemRepository itemRepo;
        private const string SEARCH_RESULTS = "Showing {0} items";
        private const string ITEM_ID = "Item ID: ";
        private const string ITEM_NAME = "Item Name: ";
        private MainPage parent;
        private Item selectedItem { get; set; }
        public User CurrentUser { get; set; }

        public AllItemsForm()
        {
            InitializeComponent();
            this.itemRepo = new ItemRepository();
            this.RefreshDataGridView(this.itemRepo.ReadAll());
        }

        private void AllItemsForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
        }

        public void RefreshDataGridView(List<Item> items = null)
        {
            if (items == null || !items.Any())
            {
                this.dataGridViewAllItems.DataSource = null;
                this.labelSearchResults.Text = string.Format(SEARCH_RESULTS, "0");
            }
            else
            {
                this.refreshDataGridView(items);
                this.labelSearchResults.Text = string.Format(SEARCH_RESULTS, items.Count.ToString());
            }
        }

        private void refreshDataGridView(List<Item> items)
        {
            this.dataGridViewAllItems.DataSource = null;
            var sortedItems = new SortableBindingList<Item>(items);
            this.dataGridViewAllItems.DataSource = sortedItems;
            this.dataGridViewAllItems.Columns["AddedByID"].Visible = false;
            this.dataGridViewAllItems.Columns["ModifiedByID"].Visible = false;
            this.dataGridViewAllItems.Columns["AddedByUser"].HeaderText = "Added By";
            this.dataGridViewAllItems.Columns["ModifiedByUser"].HeaderText = "Modified By";
        }

        private void dataGridViewAllItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var rowIndex = e.RowIndex;
            var columnIndex = e.ColumnIndex;
            if (rowIndex < 0)
            {
                this.sort(columnIndex);
            } else
            {
                this.selectItem(rowIndex);
            }
        }

        private void sort(int columnIndex)
        {
            var columnToSort = this.dataGridViewAllItems.Columns[columnIndex];
            this.dataGridViewAllItems.Sort(columnToSort, ListSortDirection.Ascending);
        }

        private void selectItem(int rowIndex)
        {
            var row = this.dataGridViewAllItems.Rows[rowIndex];
            this.selectedItem = new Item()
            {
                ItemID = int.Parse(row.Cells[0].Value.ToString()),
                Name = row.Cells[1].Value.ToString(),
                SerialNumber = row.Cells[2].Value.ToString(),
                DateAdded = row.Cells[3].Value.ToString(),
                AddedByUser = row.Cells[5].Value.ToString(),
                LastModified = row.Cells[6].Value.ToString(),
                ModifiedByID = int.Parse(row.Cells[7].Value.ToString()),
                ModifiedByUser = row.Cells[8].Value.ToString(),
                Location = row.Cells[9].Value.ToString(),
                Photo = row.Cells[10].Value as Image
            };

            this.labelItemID.Text = ITEM_ID + this.selectedItem.ItemID;
            this.labelItemName.Text = ITEM_NAME + this.selectedItem.Name;
            
            this.buttonEdit.Enabled = true;
            this.buttonDelete.Enabled = true;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            this.displayEditForm();
        }

        private void displayEditForm()
        {
            var editForm = new EditItemForm();
            editForm.MdiParent = this.MdiParent;
            editForm.Dock = DockStyle.Fill;
            this.selectedItem.ModifiedByID = this.CurrentUser.UserID;
            editForm.Item = this.selectedItem;
            editForm.Show();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure you want to delete this item?", "Confirm Deletion", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.itemRepo.Destroy(this.selectedItem.ItemID);
                this.RefreshDataGridView();
                labelItemID.Text = ITEM_ID;
                labelItemName.Text = ITEM_NAME;
                this.selectedItem = null;
                this.buttonEdit.Enabled = false;
                this.buttonDelete.Enabled = false;
            }
        }

        private void AllItemsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            this.executeSearch();
        }

        private void executeSearch()
        {
            var foundItems = new List<Item>();
            var searchTerm = this.textBoxSearch.Text.ToLower();
            var allItems = this.itemRepo.ReadAll();
            foreach (var item in allItems)
            {
                if (item.Name.ToLower().Contains(searchTerm))
                {
                    foundItems.Add(item);
                }
                var addedByUserMatches = item.AddedByUser.ToLower().Contains(searchTerm);
                var modifiedByUserMatches = item.ModifiedByUser.ToLower().Contains(searchTerm);
                if (addedByUserMatches || modifiedByUserMatches)
                {
                    foundItems.Add(item);
                }
                if (item.Location.ToLower().Contains(searchTerm))
                {
                    foundItems.Add(item);
                }
                if (item.SerialNumber.ToLower().Contains(searchTerm))
                {
                    foundItems.Add(item);
                }
            }
            this.RefreshDataGridView(foundItems);
        }

        private void buttonShowAll_Click(object sender, EventArgs e)
        {
            var allItems = this.itemRepo.ReadAll();
            this.refreshDataGridView(allItems);
            this.labelSearchResults.Text = string.Format(SEARCH_RESULTS, allItems.Count.ToString());
        }

        private void textBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.executeSearch();
            }
        }
    }
}
