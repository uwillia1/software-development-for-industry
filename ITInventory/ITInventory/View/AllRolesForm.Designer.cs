﻿namespace ITInventory
{
    partial class AllRolesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewAllRoles = new System.Windows.Forms.DataGridView();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelInstructions = new System.Windows.Forms.Label();
            this.labelRoleID = new System.Windows.Forms.Label();
            this.labelRoleName = new System.Windows.Forms.Label();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllRoles)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewAllRoles
            // 
            this.dataGridViewAllRoles.AllowUserToAddRows = false;
            this.dataGridViewAllRoles.AllowUserToDeleteRows = false;
            this.dataGridViewAllRoles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewAllRoles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAllRoles.Location = new System.Drawing.Point(13, 40);
            this.dataGridViewAllRoles.Name = "dataGridViewAllRoles";
            this.dataGridViewAllRoles.ReadOnly = true;
            this.dataGridViewAllRoles.RowHeadersVisible = false;
            this.dataGridViewAllRoles.Size = new System.Drawing.Size(240, 150);
            this.dataGridViewAllRoles.TabIndex = 0;
            this.dataGridViewAllRoles.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAllRoles_CellDoubleClick);
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(94, 276);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 23);
            this.buttonBack.TabIndex = 1;
            this.buttonBack.Text = "Close";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelInstructions
            // 
            this.labelInstructions.AutoSize = true;
            this.labelInstructions.Location = new System.Drawing.Point(10, 9);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(203, 13);
            this.labelInstructions.TabIndex = 2;
            this.labelInstructions.Text = "Double click a row to edit or delete a role.";
            // 
            // labelRoleID
            // 
            this.labelRoleID.AutoSize = true;
            this.labelRoleID.Location = new System.Drawing.Point(30, 200);
            this.labelRoleID.Name = "labelRoleID";
            this.labelRoleID.Size = new System.Drawing.Size(49, 13);
            this.labelRoleID.TabIndex = 3;
            this.labelRoleID.Text = "Role ID: ";
            // 
            // labelRoleName
            // 
            this.labelRoleName.AutoSize = true;
            this.labelRoleName.Location = new System.Drawing.Point(13, 219);
            this.labelRoleName.Name = "labelRoleName";
            this.labelRoleName.Size = new System.Drawing.Size(66, 13);
            this.labelRoleName.TabIndex = 4;
            this.labelRoleName.Text = "Role Name: ";
            // 
            // buttonEdit
            // 
            this.buttonEdit.Enabled = false;
            this.buttonEdit.Location = new System.Drawing.Point(178, 195);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonEdit.TabIndex = 5;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Enabled = false;
            this.buttonDelete.Location = new System.Drawing.Point(178, 224);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 6;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // AllRolesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 308);
            this.ControlBox = false;
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.labelRoleName);
            this.Controls.Add(this.labelRoleID);
            this.Controls.Add(this.labelInstructions);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.dataGridViewAllRoles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AllRolesForm";
            this.Text = "All Roles";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AllRolesForm_FormClosing);
            this.Load += new System.EventHandler(this.AllRolesForm_Load);
            this.LocationChanged += new System.EventHandler(this.AllRoles_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllRoles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewAllRoles;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label labelInstructions;
        private System.Windows.Forms.Label labelRoleID;
        private System.Windows.Forms.Label labelRoleName;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDelete;
    }
}