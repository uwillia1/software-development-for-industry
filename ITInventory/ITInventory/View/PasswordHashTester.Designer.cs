﻿namespace ITInventory
{
    partial class PasswordHashTester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelInputPassword = new System.Windows.Forms.Label();
            this.textBoxInputPassword = new System.Windows.Forms.TextBox();
            this.labelOutputHash = new System.Windows.Forms.Label();
            this.textBoxOutputHash = new System.Windows.Forms.TextBox();
            this.buttonHash = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelInputPassword
            // 
            this.labelInputPassword.AutoSize = true;
            this.labelInputPassword.Location = new System.Drawing.Point(13, 14);
            this.labelInputPassword.Name = "labelInputPassword";
            this.labelInputPassword.Size = new System.Drawing.Size(80, 13);
            this.labelInputPassword.TabIndex = 0;
            this.labelInputPassword.Text = "Input Password";
            // 
            // textBoxInputPassword
            // 
            this.textBoxInputPassword.Location = new System.Drawing.Point(16, 30);
            this.textBoxInputPassword.Name = "textBoxInputPassword";
            this.textBoxInputPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxInputPassword.TabIndex = 1;
            // 
            // labelOutputHash
            // 
            this.labelOutputHash.AutoSize = true;
            this.labelOutputHash.Location = new System.Drawing.Point(13, 73);
            this.labelOutputHash.Name = "labelOutputHash";
            this.labelOutputHash.Size = new System.Drawing.Size(67, 13);
            this.labelOutputHash.TabIndex = 2;
            this.labelOutputHash.Text = "Output Hash";
            // 
            // textBoxOutputHash
            // 
            this.textBoxOutputHash.Location = new System.Drawing.Point(16, 89);
            this.textBoxOutputHash.Name = "textBoxOutputHash";
            this.textBoxOutputHash.ReadOnly = true;
            this.textBoxOutputHash.Size = new System.Drawing.Size(718, 20);
            this.textBoxOutputHash.TabIndex = 3;
            // 
            // buttonHash
            // 
            this.buttonHash.Location = new System.Drawing.Point(133, 28);
            this.buttonHash.Name = "buttonHash";
            this.buttonHash.Size = new System.Drawing.Size(75, 23);
            this.buttonHash.TabIndex = 4;
            this.buttonHash.Text = "Hash";
            this.buttonHash.UseVisualStyleBackColor = true;
            this.buttonHash.Click += new System.EventHandler(this.buttonHash_Click);
            // 
            // PasswordHashTester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 129);
            this.Controls.Add(this.buttonHash);
            this.Controls.Add(this.textBoxOutputHash);
            this.Controls.Add(this.labelOutputHash);
            this.Controls.Add(this.textBoxInputPassword);
            this.Controls.Add(this.labelInputPassword);
            this.Name = "PasswordHashTester";
            this.Text = "PasswordHashTester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInputPassword;
        private System.Windows.Forms.TextBox textBoxInputPassword;
        private System.Windows.Forms.Label labelOutputHash;
        private System.Windows.Forms.TextBox textBoxOutputHash;
        private System.Windows.Forms.Button buttonHash;
    }
}