﻿using ITInventory.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ITInventory.Controller;
using ITInventory.View;
using ITInventory.Model;

namespace ITInventory
{
    public partial class AllRolesForm : Form
    {
        private const string ROLE_ID_HEADER = "RoleID";
        private UserManager manager;
        private MainPage parent;
        private User currentUser;

        private int roleID { get; set; }
        private string roleName { get; set; }

        public AllRolesForm()
        {
            InitializeComponent();
            this.RefreshDataGridView();
            this.dataGridViewAllRoles.Columns[ROLE_ID_HEADER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.manager = new UserManager();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (roleName != "Admin")
            {
                var result = MessageBox.Show("Are you sure you want to delete role " + roleName + " ?", "Confirm Deletion", MessageBoxButtons.YesNo);
                checkDialogResult(result);
            }
        }

        private void checkDialogResult(DialogResult result)
        {
            if (result == DialogResult.Yes)
            {
                manager.deleteRole(roleID);
                this.RefreshDataGridView();
                labelRoleID.Text = "Role ID: ";
                labelRoleName.Text = "Role Name: ";
            }
        }

        public void RefreshDataGridView()
        {
            this.dataGridViewAllRoles.DataSource = null;
            this.dataGridViewAllRoles.DataSource = new SortableBindingList<Role>(new RoleRepository().ReadAll());
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (roleName != "Admin")
            {
                var editRoleForm = new EditRoleForm();
                editRoleForm.MdiParent = this.MdiParent;
                editRoleForm.Dock = DockStyle.Fill;
                editRoleForm.roleID = this.roleID;
                editRoleForm.roleName = this.roleName;
                editRoleForm.Show();
                this.dataGridViewAllRoles.DataSource = null;
                this.dataGridViewAllRoles.DataSource = new RoleRepository().ReadAll();
                this.labelRoleID.Text = "Role ID: ";
                this.labelRoleName.Text = "Role Name: ";
            }
        }

        private void AllRoles_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void dataGridViewAllRoles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                this.sort(e.ColumnIndex);
            } else
            {
                this.selectRole(e);
            }
        }

        private void sort(int columnIndex)
        {
            var columnToSort = this.dataGridViewAllRoles.Columns[columnIndex];
            this.dataGridViewAllRoles.Sort(columnToSort, ListSortDirection.Ascending);
        }

        private void selectRole(DataGridViewCellEventArgs e)
        {
            if (this.currentUser.Role.Name == "Admin")
            {
                buttonEdit.Enabled = true;
                buttonDelete.Enabled = true;
            }

            var row = dataGridViewAllRoles.Rows[e.RowIndex];
            roleID = int.Parse(row.Cells[0].Value.ToString());
            roleName = row.Cells[1].Value.ToString();

            labelRoleID.Text = "Role ID: " + roleID;
            labelRoleName.Text = "Role Name: " + roleName;
        }

        private void AllRolesForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
            this.currentUser = this.parent.CurrentUser;
        }

        private void AllRolesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }
    }
}
