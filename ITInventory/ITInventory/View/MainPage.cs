﻿using ITInventory.Controller;
using ITInventory.Utility;
using ITInventory.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ITInventory
{
    /// <summary>
    /// The Main Page is the form the user will interact with to perform all operations.
    /// Operations include, logging in and out and CRUD operations.
    /// </summary>
    public partial class MainPage : Form
    {
        private Authenticator authenticator;
        private UserManager userManager;
        private const string labelCurrentUserDefaultText = "Welcome ";
        public User CurrentUser { get; set; }
        private List<Form> openForms;
        public List<Form> OpenForms {
            get
            {
                return this.openForms;
            }
            private set
            {
                this.openForms = value;
            }
        }

        /// <summary>
        /// Creates a new instance of the Main Page Form
        /// </summary>
        public MainPage()
        {
            this.authenticator = new Authenticator();
            this.userManager = new UserManager();
            InitializeComponent();
            this.enableToolStripItemsForAdmin(false);
            this.openForms = new List<Form>();
        }

        private void enableToolStripItemsForAdmin(bool enabled)
        {
            this.createToolStripMenuItem.Enabled = enabled;
            this.newRoleToolStripMenuItem.Enabled = enabled;
            this.enableToolStripItemsForNonAdmin(enabled);
        }

        private void enableToolStripItemsForNonAdmin(bool enabled)
        {
            this.changePasswordToolStripMenuItem.Enabled = enabled;
            this.showAllToolStripMenuItem.Enabled = enabled;
            this.showAllToolStripMenuItem1.Enabled = enabled;
            this.newItemToolStripMenuItem.Enabled = enabled;
            this.showAllToolStripMenuItem2.Enabled = enabled;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            this.login();
        }

        private void login()
        {
            var username = this.textBoxUsername.Text;
            if (!this.authenticator.CheckAccountEnabled(username))
            {
                MessageBox.Show("Your account has been disabled.\r\n\r\nPlease contact your system administrator.");
                return;
            }
            var loginIsValid = this.authenticator.ValidateLogin(this.textBoxUsername.Text, this.textBoxPassword.Text);
            var userRole = userManager.GetUserRole(this.textBoxUsername.Text);
            if (loginIsValid)
            {
                MessageBox.Show("You have successfully logged in!");
                this.CurrentUser = userManager.readUser(this.textBoxUsername.Text);
                this.labelCurrentUser.Text += this.textBoxUsername.Text;
                this.textBoxUsername.Text = string.Empty;
                this.textBoxPassword.Text = string.Empty;
                this.toolStripLogin.Hide();
                this.toolStripCurrentUser.Show();

                if (userRole.Name == "Admin")
                {
                    this.enableToolStripItemsForAdmin(true);
                }
                else
                {
                    this.enableToolStripItemsForNonAdmin(true);
                }
            }
            else
            {
                MessageBox.Show("Username or password incorrect." + "\r\n\r\n" + "Please try again.");
            }
        }

        private void toolStripLogOut_Click(object sender, EventArgs e)
        {
            var confirmLogOutDialog = MessageBox.Show("Are you sure you want to log out?", "Log Out", MessageBoxButtons.YesNo);
            if (confirmLogOutDialog == DialogResult.Yes)
            {
                this.labelCurrentUser.Text = labelCurrentUserDefaultText;
                this.toolStripCurrentUser.Hide();
                this.toolStripLogin.Show();
                this.enableToolStripItemsForAdmin(false);
                this.closeAllChildForms();
            }
        }

        private void closeAllChildForms()
        {
            for (int i = this.openForms.Count - 1; i >= 0; i--)
            {
                this.openForms[i].Close();
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.confirmExit();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewUserForm newUserForm = new NewUserForm();
            this.ShowNewForm(newUserForm);
        }

        public void ShowNewForm(Form form)
        {
            if (!OpenFormManager.isFormOpen(this.openForms, form))
            {
                this.openForms.Add(form);
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.Show();
            }
            else
            {
                form.Dispose();
            }
        }

        private void newRoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewRoleForm newRoleForm = new NewRoleForm();
            this.ShowNewForm(newRoleForm);
        }

        private void showAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllUsersForm allUsersForm = new AllUsersForm();
            this.ShowNewForm(allUsersForm);
        }

        private void showAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AllRolesForm allRolesForm = new AllRolesForm();
            this.ShowNewForm(allRolesForm);
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.confirmExit();
        }

        private void confirmExit()
        {
            var confirmExitDialog = MessageBox.Show("Are you sure you want to exit the application?", "Exit", MessageBoxButtons.YesNo);
            if (confirmExitDialog == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void buttonExitLogInToolStrip_Click(object sender, EventArgs e)
        {
            this.confirmExit();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordForm changePasswordForm = new ChangePasswordForm();
            changePasswordForm.Text = this.labelCurrentUser.Text.Split(' ')[1];
            this.ShowNewForm(changePasswordForm);
        }

        private void MainPage_Load(object sender, EventArgs e)
        {
            TextBox passwordBox = this.textBoxPassword.Control as TextBox;
            passwordBox.UseSystemPasswordChar = true;
        }

        private void textBoxUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.login();
            }
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.login();
            }
        }

        private void newItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewItemForm newItemForm = new NewItemForm();
            newItemForm.CurrentUser = this.CurrentUser;
            this.ShowNewForm(newItemForm);
        }

        private void showAllToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AllItemsForm allItemsForm = new AllItemsForm();
            allItemsForm.CurrentUser = this.CurrentUser;
            this.ShowNewForm(allItemsForm);
        }
    }
}
