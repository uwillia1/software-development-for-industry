﻿using ITInventory.DAL;
using ITInventory.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory.View
{
    public partial class EditItemForm : Form
    {
        private const string ITEM_ID = "Item ID: ";
        private const string ITEM_NAME = "Item Name: ";
        private const string SERIAL_NUMBER = "Serial #: ";
        private const string DATE_ADDED = "Date Added: ";
        private const string ADDED_BY = "Added By: ";
        private const string LAST_MODIFIED = "Last Modified: ";
        private const string MODIFIED_BY = "Modified By: ";
        private const string LOCATION = "Location: ";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        public Item Item { get; set; }

        private ItemRepository itemRepo;
        private MainPage parent;

        private const string LABEL_PHOTO = "Photo: ";
        private const long ONE_MB = 1048576;

        public EditItemForm()
        {
            InitializeComponent();
            this.itemRepo = new ItemRepository();
        }

        private void EditItemForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
            this.labelItemID.Text = ITEM_ID + Item.ItemID;
            this.labelItemName.Text = ITEM_NAME + Item.Name;
            this.labelSerialNumber.Text = SERIAL_NUMBER + Item.SerialNumber;
            this.labelDateAdded.Text = DATE_ADDED + Item.DateAdded;
            this.labelAddedBy.Text = ADDED_BY + Item.AddedByUser;
            this.labelLastModified.Text = LAST_MODIFIED + Item.LastModified;
            this.labelModifiedBy.Text = MODIFIED_BY + Item.ModifiedByUser;
            this.labelLocation.Text = LOCATION;
            this.textBoxLocation.Text = Item.Location;
            this.pictureBox.Image = Item.Photo;
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            this.updateItem();
            this.refreshAllItemsForm();
        }

        private void updateItem()
        {
            if (string.IsNullOrWhiteSpace(this.textBoxLocation.Text))
            {
                MessageBox.Show("Item must have a location");
            }
            var item = this.Item;
            item.Location = this.textBoxLocation.Text;
            item.LastModified = DateTime.Today.ToString(DATE_FORMAT);
            item.ModifiedByID = Item.ModifiedByID;
            item.Photo = this.pictureBox.Image;
            this.itemRepo.Update(item);
            MessageBox.Show("Item has been updated!");
            this.Close();
        }

        private void refreshAllItemsForm()
        {
            var openForms = this.parent.OpenForms;
            foreach (var form in openForms)
            {
                if (form.GetType() == typeof(AllItemsForm))
                {
                    var allItemsForm = form as AllItemsForm;
                    allItemsForm.RefreshDataGridView();
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditItemForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }

        private void buttonOpenPhoto_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            fileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                var fileName = fileDialog.FileName;
                var fileSize = new FileInfo(fileName).Length;
                if (fileSize > ONE_MB)
                {
                    MessageBox.Show("Image must be smaller than 1 MB");
                }
                else
                {
                    this.labelPhoto.Text = LABEL_PHOTO + fileDialog.FileName;
                    var photo = new Bitmap(fileDialog.FileName);
                    this.pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    this.pictureBox.Image = photo;
                }

            }
        }
    }
}
