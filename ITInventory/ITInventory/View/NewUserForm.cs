﻿using ITInventory.Controller;
using ITInventory.DAL;
using ITInventory.Model;
using System;
using System.Windows.Forms;
using System.Drawing;

namespace ITInventory
{
    /// <summary>
    /// Renders a form to add a new user to the database.
    /// </summary>
    public partial class NewUserForm : Form
    {
        private UserManager userManager;
        private UserRepository userRepo;
        private RoleRepository rolerepo;
        private BindingSource roles;
        private MainPage parent;

        /// <summary>
        /// Creates a new instance of the New User Form.
        /// </summary>
        public NewUserForm()
        {
            this.userManager = new UserManager();
            this.userRepo = new UserRepository();
            this.rolerepo = new RoleRepository();
            this.roles = new BindingSource();
            InitializeComponent();
            this.roles.DataSource = this.rolerepo.ReadAll();
            this.comboBoxRoles.DataSource = this.roles.DataSource;
            comboBoxRoles.DisplayMember = "Name";
            comboBoxRoles.ValueMember = "Name";
        }

        private void NewUserForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
        }

        private void NewUserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var usernameTaken = this.usernameIsTaken(this.textBoxUsername.Text);
            var emailTaken = this.emailIsTaken(this.textBoxEmail.Text);
            var passwordsMatch = this.passwordsMatch(this.textBoxPassword.Text, this.textBoxConfirmPassword.Text);
            if (usernameTaken)
            {
                MessageBox.Show("That username is taken.");
            }
            else if (emailTaken)
            {
                MessageBox.Show("That email is taken.");
            }
            else if (!passwordsMatch)
            {
                MessageBox.Show("Passwords do not match.");
            }
            else
            {
                this.userManager.AddNewUser(this.textBoxUsername.Text, this.textBoxPassword.Text, this.textBoxFirstName.Text, this.textBoxLastName.Text, this.textBoxEmail.Text, (Role)this.comboBoxRoles.SelectedItem);
                this.refreshAllUsersForm();
                MessageBox.Show("Added new user!");
                this.Close();
            }
            
        }

        private bool usernameIsTaken(string username)
        {
            foreach (var user in this.userRepo.ReadAll())
            {
                if (user.Username == username)
                {
                    return true;
                }
            }
            return false;
        }

        private bool emailIsTaken(string email)
        {
            foreach (var user in this.userRepo.ReadAll())
            {
                if (user.Email == email)
                {
                    return true;
                }
            }
            return false;
        }

        private bool passwordsMatch(string password, string confirmedPassword)
        {
            return password == confirmedPassword;
        }

        private void refreshAllUsersForm()
        {
            var openForms = this.parent.OpenForms;
            foreach (var form in openForms)
            {
                if (form.GetType() == typeof(AllUsersForm))
                {
                    var allUsersForm = form as AllUsersForm;
                    allUsersForm.RefreshDataGridView();
                }
            }
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewUserForm_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0,0);
        }
    }
}
