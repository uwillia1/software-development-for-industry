﻿using ITInventory.DAL;
using ITInventory.Model;
using ITInventory.Utility;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ITInventory.View
{
    public partial class NewItemForm : Form
    {
        private const string DATE_FORMAT = "yyyy-MM-dd";
        public User CurrentUser { get; set; }
        private ItemRepository itemRepo;
        private MainPage parent;
        private const string LABEL_PHOTO = "Photo: ";
        private const long ONE_MB = 1048576;

        public NewItemForm()
        {
            InitializeComponent();
            this.itemRepo = new ItemRepository();
        }

        private void NewItemForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
        }

        private void NewItemForm_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            this.submitData();
            this.showAllItemsForm();
        }

        private void submitData()
        {
            if (this.textBoxName.Text == string.Empty)
            {
                MessageBox.Show("Please enter item name");
            }
            if (this.textBoxLocation.Text == string.Empty)
            {
                MessageBox.Show("Please enter item location");
            }
            var currentDate = DateTime.Today.ToString(DATE_FORMAT);
            var item = new Item()
            {
                Name = this.textBoxName.Text,
                SerialNumber = this.textBoxSerialNumber.Text,
                DateAdded = currentDate,
                AddedByID = this.CurrentUser.UserID,
                LastModified = currentDate,
                ModifiedByID = this.CurrentUser.UserID,
                Location = this.textBoxLocation.Text
            };
            if (this.pictureBox.Image != null)
            {
                item.Photo = this.pictureBox.Image;
            }
            else
            {
                item.Photo = Properties.Resources.no_image;
            }
            this.itemRepo.Create(item);
            this.Close();
        }

        private void showAllItemsForm()
        {
            var openForms = this.parent.OpenForms;
            var allItemsIsOpen = false;
            foreach (var form in openForms)
            {
                if (form.GetType() == typeof(AllItemsForm))
                {
                    allItemsIsOpen = true;
                    var allItemsForm = form as AllItemsForm;
                    allItemsForm.RefreshDataGridView();
                }
            }
            if (!allItemsIsOpen)
            {
                var allItemsForm = new AllItemsForm();
                allItemsForm.CurrentUser = this.CurrentUser;
                this.parent.ShowNewForm(allItemsForm);
            }
        }

        private void textBoxName_KeyDown(object sender, KeyEventArgs e)
        {
            whenEnterIsPressed(e);
        }

        private void whenEnterIsPressed(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.submitData();
            }
        }

        private void textBoxSerialNumber_KeyDown(object sender, KeyEventArgs e)
        {
            this.whenEnterIsPressed(e);
        }

        private void textBoxLocation_KeyDown(object sender, KeyEventArgs e)
        {
            this.whenEnterIsPressed(e);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOpenPhoto_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            fileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                var fileName = fileDialog.FileName;
                var fileSize = new FileInfo(fileName).Length;
                if (fileSize > ONE_MB)
                {
                    MessageBox.Show("Image must be smaller than 1 MB");
                } else
                {
                    this.labelPhoto.Text = LABEL_PHOTO + fileDialog.FileName;
                    var photo = new Bitmap(fileDialog.FileName);
                    this.pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    this.pictureBox.Image = photo;
                }
                
            }
        }

        private void NewItemForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }
    }
}
