﻿using ITInventory.Controller;
using ITInventory.DAL;
using ITInventory.Model;
using ITInventory.View;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ITInventory
{
    public partial class AllUsersForm : Form
    {
        private User selectedUser { get; set; }
        private int userID { get; set; }
        private string username { get; set; }
        private string firstName { get; set; }
        private string lastName { get; set; }
        private string email { get; set; }
        private bool enabled { get; set; }

        private UserManager manager;
        private MainPage parent;
        private User currentUser;

        public AllUsersForm()
        {
            InitializeComponent();
            this.manager = new UserManager();
            RefreshDataGridView();
        }

        private void AllUsersForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
            this.currentUser = this.parent.CurrentUser;
        }

        private void AllUsersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }

        public void RefreshDataGridView()
        {
            this.dataGridViewAllUsers.DataSource = null;
            var allUsers = new SortableBindingList<User>(new UserRepository().ReadAll());
            this.dataGridViewAllUsers.DataSource = allUsers;
            this.dataGridViewAllUsers.Columns["Role"].Visible = false;
            this.dataGridViewAllUsers.Columns["RoleName"].HeaderText = "Role";
            this.dataGridViewAllUsers.Columns["Enabled"].SortMode = DataGridViewColumnSortMode.Automatic;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (username != "admin")
            {
                var result = MessageBox.Show("Are you sure you want to disable user " + username + " ?", "Confirm Disable", MessageBoxButtons.YesNo);
                disableUser(result);
            }
            this.RefreshDataGridView();
        }

        private void disableUser(DialogResult result)
        {
            if (result == DialogResult.Yes)
            {
                manager.DisableUser(userID);
                this.dataGridViewAllUsers.DataSource = null;
                this.dataGridViewAllUsers.DataSource = new UserRepository().ReadAll();
                labelUserID.Text = "User ID: ";
                labelUsername.Text = "Username: ";
            }
        }

        private void dataGridViewAllUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                this.sort(e.ColumnIndex);
            }
            else
            {
                this.selectUser(e);
            }
        }

        private void sort(int columnIndex)
        {
            var columnToSort = this.dataGridViewAllUsers.Columns[columnIndex];
            this.dataGridViewAllUsers.Sort(columnToSort, ListSortDirection.Ascending);
        }

        private void selectUser(DataGridViewCellEventArgs e)
        {
            if (this.currentUser.Role.Name == "Admin")
            {
                buttonEdit.Enabled = true;
            }

            var rowIndex = e.RowIndex;
            var row = dataGridViewAllUsers.Rows[rowIndex];

            this.selectedUser = new User
            {
                UserID = int.Parse(row.Cells[0].Value.ToString()),
                Username = row.Cells[1].Value.ToString(),
                FirstName = row.Cells[2].Value.ToString(),
                LastName = row.Cells[3].Value.ToString(),
                Email = row.Cells[4].Value.ToString(),
                RoleName = row.Cells[6].Value.ToString(),
                Enabled = (bool)row.Cells[7].Value
            };
            labelUserID.Text = "User ID: " + selectedUser.UserID;
            labelUsername.Text = "Username: " + selectedUser.Username;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (username != "admin")
            {
                var editUserForm = new EditUserForm();
                editUserForm.MdiParent = this.MdiParent;
                editUserForm.Dock = DockStyle.Fill;

                editUserForm.userToEdit = this.selectedUser;

                editUserForm.Show();
                editUserForm.FormClosed += new FormClosedEventHandler(EditUserForm_Closed);
                
                this.labelUserID.Text = "Role ID: ";
                this.labelUsername.Text = "Role Name: ";
            }
        }

        void EditUserForm_Closed(object sender, FormClosedEventArgs e)
        {
            EditUserForm editUserForm = (EditUserForm)sender;
            this.RefreshDataGridView();
        }

        private void AllUsers_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
