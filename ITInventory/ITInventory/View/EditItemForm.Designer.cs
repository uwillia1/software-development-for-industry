﻿namespace ITInventory.View
{
    partial class EditItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelItemID = new System.Windows.Forms.Label();
            this.labelItemName = new System.Windows.Forms.Label();
            this.labelSerialNumber = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.labelDateAdded = new System.Windows.Forms.Label();
            this.labelAddedBy = new System.Windows.Forms.Label();
            this.labelLastModified = new System.Windows.Forms.Label();
            this.labelModifiedBy = new System.Windows.Forms.Label();
            this.textBoxLocation = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.labelPhoto = new System.Windows.Forms.Label();
            this.buttonOpenPhoto = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // labelItemID
            // 
            this.labelItemID.AutoSize = true;
            this.labelItemID.Location = new System.Drawing.Point(41, 9);
            this.labelItemID.Name = "labelItemID";
            this.labelItemID.Size = new System.Drawing.Size(47, 13);
            this.labelItemID.TabIndex = 0;
            this.labelItemID.Text = "Item ID: ";
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(24, 31);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(64, 13);
            this.labelItemName.TabIndex = 1;
            this.labelItemName.Text = "Item Name: ";
            // 
            // labelSerialNumber
            // 
            this.labelSerialNumber.AutoSize = true;
            this.labelSerialNumber.Location = new System.Drawing.Point(39, 54);
            this.labelSerialNumber.Name = "labelSerialNumber";
            this.labelSerialNumber.Size = new System.Drawing.Size(49, 13);
            this.labelSerialNumber.TabIndex = 2;
            this.labelSerialNumber.Text = "Serial #: ";
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Location = new System.Drawing.Point(34, 170);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(54, 13);
            this.labelLocation.TabIndex = 3;
            this.labelLocation.Text = "Location: ";
            // 
            // labelDateAdded
            // 
            this.labelDateAdded.AutoSize = true;
            this.labelDateAdded.Location = new System.Drawing.Point(18, 76);
            this.labelDateAdded.Name = "labelDateAdded";
            this.labelDateAdded.Size = new System.Drawing.Size(70, 13);
            this.labelDateAdded.TabIndex = 4;
            this.labelDateAdded.Text = "Date Added: ";
            // 
            // labelAddedBy
            // 
            this.labelAddedBy.AutoSize = true;
            this.labelAddedBy.Location = new System.Drawing.Point(29, 98);
            this.labelAddedBy.Name = "labelAddedBy";
            this.labelAddedBy.Size = new System.Drawing.Size(59, 13);
            this.labelAddedBy.TabIndex = 5;
            this.labelAddedBy.Text = "Added By: ";
            // 
            // labelLastModified
            // 
            this.labelLastModified.AutoSize = true;
            this.labelLastModified.Location = new System.Drawing.Point(12, 121);
            this.labelLastModified.Name = "labelLastModified";
            this.labelLastModified.Size = new System.Drawing.Size(76, 13);
            this.labelLastModified.TabIndex = 6;
            this.labelLastModified.Text = "Last Modified: ";
            // 
            // labelModifiedBy
            // 
            this.labelModifiedBy.AutoSize = true;
            this.labelModifiedBy.Location = new System.Drawing.Point(20, 147);
            this.labelModifiedBy.Name = "labelModifiedBy";
            this.labelModifiedBy.Size = new System.Drawing.Size(68, 13);
            this.labelModifiedBy.TabIndex = 7;
            this.labelModifiedBy.Text = "Modified By: ";
            // 
            // textBoxLocation
            // 
            this.textBoxLocation.Location = new System.Drawing.Point(94, 167);
            this.textBoxLocation.Name = "textBoxLocation";
            this.textBoxLocation.Size = new System.Drawing.Size(100, 20);
            this.textBoxLocation.TabIndex = 15;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(38, 325);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 16;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(119, 325);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 17;
            this.buttonSubmit.Text = "Submit";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // labelPhoto
            // 
            this.labelPhoto.AutoSize = true;
            this.labelPhoto.Location = new System.Drawing.Point(46, 197);
            this.labelPhoto.Name = "labelPhoto";
            this.labelPhoto.Size = new System.Drawing.Size(41, 13);
            this.labelPhoto.TabIndex = 18;
            this.labelPhoto.Text = "Photo: ";
            // 
            // buttonOpenPhoto
            // 
            this.buttonOpenPhoto.Location = new System.Drawing.Point(12, 213);
            this.buttonOpenPhoto.Name = "buttonOpenPhoto";
            this.buttonOpenPhoto.Size = new System.Drawing.Size(75, 23);
            this.buttonOpenPhoto.TabIndex = 19;
            this.buttonOpenPhoto.Text = "Open Photo";
            this.buttonOpenPhoto.UseVisualStyleBackColor = true;
            this.buttonOpenPhoto.Click += new System.EventHandler(this.buttonOpenPhoto_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(94, 213);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(100, 100);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 20;
            this.pictureBox.TabStop = false;
            // 
            // EditItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 357);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.buttonOpenPhoto);
            this.Controls.Add(this.labelPhoto);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBoxLocation);
            this.Controls.Add(this.labelModifiedBy);
            this.Controls.Add(this.labelLastModified);
            this.Controls.Add(this.labelAddedBy);
            this.Controls.Add(this.labelDateAdded);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.labelSerialNumber);
            this.Controls.Add(this.labelItemName);
            this.Controls.Add(this.labelItemID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditItemForm";
            this.Text = "EditItemForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditItemForm_FormClosing);
            this.Load += new System.EventHandler(this.EditItemForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelItemID;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.Label labelSerialNumber;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label labelDateAdded;
        private System.Windows.Forms.Label labelAddedBy;
        private System.Windows.Forms.Label labelLastModified;
        private System.Windows.Forms.Label labelModifiedBy;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Label labelPhoto;
        private System.Windows.Forms.Button buttonOpenPhoto;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}