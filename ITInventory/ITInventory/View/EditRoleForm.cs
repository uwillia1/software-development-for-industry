﻿using ITInventory.Controller;
using ITInventory.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory.View
{
    public partial class EditRoleForm : Form
    {
        private UserManager manager;
        private MainPage parent;

        public int roleID { get; set; }
        public string roleName { get; set; }

        public EditRoleForm()
        {
            InitializeComponent();
            this.manager = new UserManager();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            submitData();
            this.refreshAllRolesForm();
        }

        private void refreshAllRolesForm()
        {
            var openForms = this.parent.OpenForms;
            foreach (var form in openForms)
            {
                if (form.GetType() == typeof(AllRolesForm))
                {
                    var allRolesForm = form as AllRolesForm;
                    allRolesForm.RefreshDataGridView();
                }
            }
        }

        private void submitData()
        {
            var role = new Role()
            {
                Name = textBoxNewName.Text
            };
            role.RoleID = roleID;
            manager.updateRole(role);
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditRoleForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
            this.labelRoleID.Text = "Role ID: " + roleID;
            this.labelRoleName.Text = "Role Name: " + roleName;
        }

        private void EditRoleForm_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void textBoxNewName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.submitData();
            }
        }

        private void EditRoleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }
    }
}
