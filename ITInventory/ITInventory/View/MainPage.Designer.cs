﻿namespace ITInventory
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rolesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newRoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLogin = new System.Windows.Forms.ToolStrip();
            this.labelLogin = new System.Windows.Forms.ToolStripLabel();
            this.textBoxUsername = new System.Windows.Forms.ToolStripTextBox();
            this.textBoxPassword = new System.Windows.Forms.ToolStripTextBox();
            this.buttonLogin = new System.Windows.Forms.ToolStripButton();
            this.buttonExitLogInToolStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripCurrentUser = new System.Windows.Forms.ToolStrip();
            this.labelCurrentUser = new System.Windows.Forms.ToolStripLabel();
            this.buttonExitCurrentUserToolStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripLogOut = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1.SuspendLayout();
            this.toolStripLogin.SuspendLayout();
            this.toolStripCurrentUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.rolesToolStripMenuItem,
            this.itemsToolStripMenuItem,
            this.accountToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(835, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.showAllToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.createToolStripMenuItem.Text = "New User";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // showAllToolStripMenuItem
            // 
            this.showAllToolStripMenuItem.Name = "showAllToolStripMenuItem";
            this.showAllToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.showAllToolStripMenuItem.Text = "Show All";
            this.showAllToolStripMenuItem.Click += new System.EventHandler(this.showAllToolStripMenuItem_Click);
            // 
            // rolesToolStripMenuItem
            // 
            this.rolesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newRoleToolStripMenuItem,
            this.showAllToolStripMenuItem1});
            this.rolesToolStripMenuItem.Name = "rolesToolStripMenuItem";
            this.rolesToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.rolesToolStripMenuItem.Text = "Roles";
            // 
            // newRoleToolStripMenuItem
            // 
            this.newRoleToolStripMenuItem.Name = "newRoleToolStripMenuItem";
            this.newRoleToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.newRoleToolStripMenuItem.Text = "New Role";
            this.newRoleToolStripMenuItem.Click += new System.EventHandler(this.newRoleToolStripMenuItem_Click);
            // 
            // showAllToolStripMenuItem1
            // 
            this.showAllToolStripMenuItem1.Name = "showAllToolStripMenuItem1";
            this.showAllToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.showAllToolStripMenuItem1.Text = "Show All";
            this.showAllToolStripMenuItem1.Click += new System.EventHandler(this.showAllToolStripMenuItem1_Click);
            // 
            // itemsToolStripMenuItem
            // 
            this.itemsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newItemToolStripMenuItem,
            this.showAllToolStripMenuItem2});
            this.itemsToolStripMenuItem.Name = "itemsToolStripMenuItem";
            this.itemsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.itemsToolStripMenuItem.Text = "Items";
            // 
            // newItemToolStripMenuItem
            // 
            this.newItemToolStripMenuItem.Enabled = false;
            this.newItemToolStripMenuItem.Name = "newItemToolStripMenuItem";
            this.newItemToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newItemToolStripMenuItem.Text = "New Item";
            this.newItemToolStripMenuItem.Click += new System.EventHandler(this.newItemToolStripMenuItem_Click);
            // 
            // showAllToolStripMenuItem2
            // 
            this.showAllToolStripMenuItem2.Enabled = false;
            this.showAllToolStripMenuItem2.Name = "showAllToolStripMenuItem2";
            this.showAllToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.showAllToolStripMenuItem2.Text = "Show All";
            this.showAllToolStripMenuItem2.Click += new System.EventHandler(this.showAllToolStripMenuItem2_Click);
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changePasswordToolStripMenuItem});
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.accountToolStripMenuItem.Text = "Account";
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // toolStripLogin
            // 
            this.toolStripLogin.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelLogin,
            this.textBoxUsername,
            this.textBoxPassword,
            this.buttonLogin,
            this.buttonExitLogInToolStrip});
            this.toolStripLogin.Location = new System.Drawing.Point(0, 24);
            this.toolStripLogin.Name = "toolStripLogin";
            this.toolStripLogin.Size = new System.Drawing.Size(835, 25);
            this.toolStripLogin.TabIndex = 1;
            this.toolStripLogin.Text = "toolStrip1";
            // 
            // labelLogin
            // 
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(66, 22);
            this.labelLogin.Text = "User Login ";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(100, 25);
            this.textBoxUsername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxUsername_KeyDown);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(100, 25);
            this.textBoxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxPassword_KeyDown);
            // 
            // buttonLogin
            // 
            this.buttonLogin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonLogin.Image = ((System.Drawing.Image)(resources.GetObject("buttonLogin.Image")));
            this.buttonLogin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(44, 22);
            this.buttonLogin.Text = "Log In";
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonExitLogInToolStrip
            // 
            this.buttonExitLogInToolStrip.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonExitLogInToolStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonExitLogInToolStrip.Image = ((System.Drawing.Image)(resources.GetObject("buttonExitLogInToolStrip.Image")));
            this.buttonExitLogInToolStrip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonExitLogInToolStrip.Name = "buttonExitLogInToolStrip";
            this.buttonExitLogInToolStrip.Size = new System.Drawing.Size(29, 22);
            this.buttonExitLogInToolStrip.Text = "Exit";
            this.buttonExitLogInToolStrip.Click += new System.EventHandler(this.buttonExitLogInToolStrip_Click);
            // 
            // toolStripCurrentUser
            // 
            this.toolStripCurrentUser.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelCurrentUser,
            this.buttonExitCurrentUserToolStrip,
            this.toolStripLogOut});
            this.toolStripCurrentUser.Location = new System.Drawing.Point(0, 49);
            this.toolStripCurrentUser.Name = "toolStripCurrentUser";
            this.toolStripCurrentUser.Size = new System.Drawing.Size(835, 25);
            this.toolStripCurrentUser.TabIndex = 2;
            this.toolStripCurrentUser.Text = "toolStrip2";
            this.toolStripCurrentUser.Visible = false;
            // 
            // labelCurrentUser
            // 
            this.labelCurrentUser.Name = "labelCurrentUser";
            this.labelCurrentUser.Size = new System.Drawing.Size(60, 22);
            this.labelCurrentUser.Text = "Welcome ";
            // 
            // buttonExitCurrentUserToolStrip
            // 
            this.buttonExitCurrentUserToolStrip.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonExitCurrentUserToolStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonExitCurrentUserToolStrip.Image = ((System.Drawing.Image)(resources.GetObject("buttonExitCurrentUserToolStrip.Image")));
            this.buttonExitCurrentUserToolStrip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonExitCurrentUserToolStrip.Name = "buttonExitCurrentUserToolStrip";
            this.buttonExitCurrentUserToolStrip.Size = new System.Drawing.Size(29, 22);
            this.buttonExitCurrentUserToolStrip.Text = "Exit";
            this.buttonExitCurrentUserToolStrip.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // toolStripLogOut
            // 
            this.toolStripLogOut.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLogOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLogOut.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLogOut.Image")));
            this.toolStripLogOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLogOut.Name = "toolStripLogOut";
            this.toolStripLogOut.Size = new System.Drawing.Size(54, 22);
            this.toolStripLogOut.Text = "Log Out";
            this.toolStripLogOut.Click += new System.EventHandler(this.toolStripLogOut_Click);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 447);
            this.Controls.Add(this.toolStripCurrentUser);
            this.Controls.Add(this.toolStripLogin);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inventory Manager";
            this.Load += new System.EventHandler(this.MainPage_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStripLogin.ResumeLayout(false);
            this.toolStripLogin.PerformLayout();
            this.toolStripCurrentUser.ResumeLayout(false);
            this.toolStripCurrentUser.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rolesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newRoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllToolStripMenuItem1;
        private System.Windows.Forms.ToolStrip toolStripLogin;
        private System.Windows.Forms.ToolStripLabel labelLogin;
        private System.Windows.Forms.ToolStripTextBox textBoxUsername;
        private System.Windows.Forms.ToolStripTextBox textBoxPassword;
        private System.Windows.Forms.ToolStripButton buttonLogin;
        private System.Windows.Forms.ToolStrip toolStripCurrentUser;
        private System.Windows.Forms.ToolStripLabel labelCurrentUser;
        private System.Windows.Forms.ToolStripButton toolStripLogOut;
        private System.Windows.Forms.ToolStripButton buttonExitCurrentUserToolStrip;
        private System.Windows.Forms.ToolStripButton buttonExitLogInToolStrip;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllToolStripMenuItem2;
    }
}