﻿using ITInventory.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory
{
    public partial class ChangePasswordForm : Form
    {
        private Authenticator authenticator;
        private UserManager userManager;
        private MainPage parent;

        public ChangePasswordForm()
        {
            this.authenticator = new Authenticator();
            this.userManager = new UserManager();
            InitializeComponent();
        }

        private void buttonChangePassword_Click(object sender, EventArgs e)
        {
            changePassword();
        }

        private void changePassword()
        {
            var username = this.Text;
            var currentPassword = this.textBoxCurrentPassword.Text;
            var newPassword = this.textBoxNewPassword.Text;
            if (this.authenticator.ValidateLogin(username, currentPassword))
            {
                if (this.textBoxNewPassword.Text == this.textBoxConfirmNewPassword.Text)
                {
                    this.userManager.ChangePassword(username, newPassword);
                    MessageBox.Show("Your password has been updated!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("New password fields do not match");
                }
            }
            else
            {
                MessageBox.Show("Current password incorrect");
            }
        }

        private void ChangePasswordView_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxCurrentPassword_KeyDown(object sender, KeyEventArgs e)
        {
            whenEnterIsPressed(e);
        }

        private void whenEnterIsPressed(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.changePassword();
            }
        }

        private void textBoxNewPassword_KeyDown(object sender, KeyEventArgs e)
        {
            this.whenEnterIsPressed(e);
        }

        private void textBoxConfirmNewPassword_KeyDown(object sender, KeyEventArgs e)
        {
            this.whenEnterIsPressed(e);
        }

        private void ChangePasswordView_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
        }

        private void ChangePasswordView_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }
    }
}
