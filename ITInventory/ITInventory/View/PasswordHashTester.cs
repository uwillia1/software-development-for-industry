﻿using ITInventory.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory
{
    public partial class PasswordHashTester : Form
    {
        private PasswordHashser hasher;

        public PasswordHashTester()
        {
            this.hasher = new PasswordHashser();
            InitializeComponent();
        }

        private void buttonHash_Click(object sender, EventArgs e)
        {
            this.textBoxOutputHash.Text = this.hasher.ComputeHash("", this.textBoxInputPassword.Text);
        }
    }
}
