﻿using ITInventory.Controller;
using ITInventory.DAL;
using ITInventory.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ITInventory.View
{
    public partial class EditUserForm : Form
    {
        public User userToEdit { get; set; }
        public int userID { get; set; }
        public string username { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public bool enabled { get; set; }
        private BindingSource roles;
        private RoleRepository roleRepo;
        private UserManager manager;
        private MainPage parent;

        public EditUserForm()
        {
            InitializeComponent();

            this.roles = new BindingSource();
            this.roleRepo = new RoleRepository();
            this.roles.DataSource = this.roleRepo.ReadAll();
            this.comboBoxRoles.DataSource = this.roles.DataSource;
            comboBoxRoles.DisplayMember = "Name";
            comboBoxRoles.ValueMember = "Name";
            this.manager = new UserManager();
        }

        private void EditUserForm_Load(object sender, EventArgs e)
        {
            this.parent = this.MdiParent as MainPage;
            this.labelUserID.Text = "User ID: " + this.userToEdit.UserID;
            this.labelUsername.Text = "Username: " + this.userToEdit.Username;
            this.labelFirstName.Text = "First Name: " + this.userToEdit.FirstName;
            this.labelLastName.Text = "Last Name: " + this.userToEdit.LastName;
            this.labelEmail.Text = "Email: " + this.userToEdit.Email;
            var index = 0;
            foreach (var role in this.roleRepo.ReadAll())
            {
                if (this.userToEdit.RoleName == role.Name)
                {
                    this.comboBoxRoles.SelectedIndex = index;
                }
                index++;
            }
            this.checkBoxEnabled.Checked = this.userToEdit.Enabled;
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            var user = new User
            {
                UserID = this.userToEdit.UserID,
                Username = this.userToEdit.Username,
                FirstName = this.userToEdit.FirstName,
                LastName = this.userToEdit.LastName,
                Email = this.userToEdit.Email,
                Role = this.comboBoxRoles.SelectedItem as Role,
                Enabled = this.checkBoxEnabled.Checked
            };
            manager.updateUser(user);
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditUserForm_LocationChanged(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
        }

        private void checkBoxEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxEnabled.Checked)
            {
                this.enabled = true;
            }
            else
            {
                this.enabled = false;
            }
        }

        private void EditUserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parent.OpenForms.Remove(this);
        }
    }
}
