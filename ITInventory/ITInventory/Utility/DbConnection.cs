﻿using MySql.Data.MySqlClient;
using System;

namespace ITInventory.DAL
{
    /// <summary>
    /// Provides interface for getting a connection string
    /// </summary>
    public static class DbConnection
    {
        /// <summary>
        /// Returns a connection to the data server
        /// </summary>
        /// <returns>A MySqlConnection object representing the connection details</returns>
        private static MySqlConnection getConnection()
        {
            string connectionString = Properties.Connections.UWGConnection;
            return new MySqlConnection(connectionString);
        }

        /// <summary>
        /// Executes a non query command to the MySql server.
        /// Can be used for create or update actions.
        /// </summary>
        /// <param name="connection">The connection to the mysql database.</param>
        /// <param name="command">The command. Must already have a command string</param>
        /// <returns>The number of rows affected</returns>
        public static int ExecuteCommandNonQuery(MySqlCommand command)
        {
            int result = 0;
            try
            {
                var connection = getConnection();
                connection.Open();
                command.Connection = connection;
                result = command.ExecuteNonQuery();
            }
            catch (MySqlException mse)
            {
                Console.WriteLine("MySql Exception: " + mse);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }
            return result;
        }

        /// <summary>
        /// Returns a MySqlDataReader for getting results from a query.
        /// Can be used in read actions.
        /// </summary>
        /// <param name="connection">The connection to the MySql database</param>
        /// <param name="command">The command to execute. Must have a command string already</param>
        /// <returns>A MySqlDataReader for reading results from a query</returns>
        public static MySqlDataReader ExecuteCommandWithReader(MySqlCommand command)
        {
            var connection = getConnection();
            connection.Open();
            command.Connection = connection;
            var reader = command.ExecuteReader();
            return reader;
        }

        /// <summary>
        /// Executes a scalar with the specified command.
        /// </summary>
        /// <param name="command">The command to execute</param>
        /// <returns>The first row of the first column retrieved</returns>
        public static object ExecuteCommandWithScalar(MySqlCommand command)
        {
            var connection = getConnection();
            connection.Open();
            command.Connection = connection;
            var scalar = command.ExecuteScalar();
            command.Dispose();
            connection.Close();
            return scalar;
        }
    }
}
