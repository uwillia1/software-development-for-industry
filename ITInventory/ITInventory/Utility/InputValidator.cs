﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITInventory.Utility
{
    public static class InputValidator
    {
        private const int MIN_PASSWORD_CHARS = 8;

        /// <summary>
        /// A username should not be empty and can only contain 
        /// lowercase letters, numbers, and the underscore character.
        /// </summary>
        /// <param name="username">Username string to validate</param>
        /// <returns>True if username is valid, false otherwise</returns>
        public static bool validateUsername(string username)
        {
            var validChars = "abcdefghijklmnopqrstuvwxyz1234567890_";
            var usernameChars = username.ToCharArray();
            if (username == string.Empty)
            {
                return false;
            }
            foreach (var character in usernameChars)
            {
                if (!validChars.Contains(character))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// A password should be at least 8 characters long, cannot 
        /// contain spaces, and must contain at least one lowercase letter, 
        /// one uppercase letter, one number, and one special character.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool validatePassword(string password)
        {
            var lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
            var uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numbers = "1234567890";
            var specialChars = "!\"#$&'()*+,-.:;<=>?@[\\]^_`{|}~";
            if (string.IsNullOrWhiteSpace(password))
            {
                return false;
            }
            if (password.Length < MIN_PASSWORD_CHARS)
            {
                return false;
            }
            foreach (var letter in lowercaseLetters)
            {
                if (!password.Contains(letter))
                {
                    return false;
                }
            }
            foreach (var letter in uppercaseLetters)
            {
                if (!password.Contains(letter))
                {
                    return false;
                }
            }
            foreach (var number in numbers)
            {
                if (!password.Contains(number))
                {
                    return false;
                }
            }
            foreach (var specialChar in specialChars)
            {
                if (!password.Contains(specialChar))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool validateEmail(string email)
        {
            return true;
        }
    }
}
