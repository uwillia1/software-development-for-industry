﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITInventory.Utility
{
    public static class OpenFormManager
    {
        public static bool isFormOpen(List<Form> openForms, Form form)
        {
            foreach (var currentForm in openForms)
            {
                if (currentForm.GetType() == form.GetType())
                {
                    return true;
                }
            }
            return false;
        }

    }
}
