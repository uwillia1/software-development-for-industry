﻿using System;
using System.Drawing;
using System.IO;

namespace ITInventory.Utility
{
    /// <summary>
    /// Converts between BLOBs and Images
    /// </summary>
    public static class BlobConverter
    {
        /// <summary>
        /// Converts an Image to a BLOB
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static string ToBlob(Image image)
        {
            var bitmap = new Bitmap(image);
            byte[] photoBytes;
            using (var photoMemoryStream = new MemoryStream())
            {
                bitmap.Save(photoMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                photoBytes = photoMemoryStream.ToArray();
            }
            var blob = Convert.ToBase64String(photoBytes);
            return blob;
        }

        /// <summary>
        /// Converts a BLOB to an Image
        /// </summary>
        /// <param name="blob"></param>
        /// <returns></returns>
        public static Image ToImage(string blob)
        {
            var photoBytes = Convert.FromBase64String(blob);
            Image image;
            using (MemoryStream photoMemoryStream = new MemoryStream(photoBytes))
            {
                image = Image.FromStream(photoMemoryStream);
            }
            return image;
        }
    }
}
