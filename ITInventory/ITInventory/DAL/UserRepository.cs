﻿using ITInventory.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ITInventory.DAL
{
    /// <summary>
    /// The UserRepository provides an interface 
    /// for performing CRUD operations on 
    /// the database.
    /// </summary>
    public class UserRepository : IRepository<User>
    {
        private string connectionString;
        private RoleRepository roleRepo;
        private const int SINGLE_ROW = 1;
        private PasswordHashser hasher;
        private const int FK_ROLE_ID_COLUMN = 7;

        private const string DEFAULT = "DEFAULT";

        private const string ATTR_USER_ID = "user_id";
        private const string ATTR_USERNAME = "username";
        private const string ATTR_SALT = "password_salt";
        private const string ATTR_HASH = "password_hash";
        private const string ATTR_FIRST_NAME = "first_name";
        private const string ATTR_LAST_NAME = "last_name";
        private const string ATTR_EMAIL = "email";
        private const string ATTR_ROLE = "fk_user_role_role_id";
        private const string ATTR_ENABLED = "enabled";

        private const string PARAM_PREFIX = "@";

        private const string PARAM_USER_ID = PARAM_PREFIX + ATTR_USER_ID;
        private const string PARAM_USERNAME = PARAM_PREFIX + ATTR_USERNAME;
        private const string PARAM_SALT = PARAM_PREFIX + ATTR_SALT;
        private const string PARAM_HASH = PARAM_PREFIX + ATTR_HASH;
        private const string PARAM_FIRST_NAME = PARAM_PREFIX + ATTR_FIRST_NAME;
        private const string PARAM_LAST_NAME = PARAM_PREFIX + ATTR_LAST_NAME;
        private const string PARAM_EMAIL = PARAM_PREFIX + ATTR_EMAIL;
        private const string PARAM_ROLE = PARAM_PREFIX + ATTR_ROLE;
        private const string PARAM_ENABLED = PARAM_PREFIX + ATTR_ENABLED;

        /// <summary>
        /// Creates a new instance of UserRepository
        /// </summary>
        public UserRepository()
        {
            this.connectionString = Properties.Connections.UWGConnection;
            this.roleRepo = new RoleRepository();
            this.hasher = new PasswordHashser();
        }

        /// <summary>
        /// Addes a new user to the database.
        /// </summary>
        /// <param name="model">The User to add</param>
        public void Create(User model)
        {
            try
            {
                var commandString = "INSERT INTO user ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}) ";
                commandString = string.Format(commandString, ATTR_USER_ID, ATTR_USERNAME, 
                    ATTR_SALT, ATTR_HASH, ATTR_FIRST_NAME, 
                    ATTR_LAST_NAME, ATTR_EMAIL, ATTR_ROLE, 
                    ATTR_ENABLED);
                commandString += "VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8});";
                commandString = string.Format(commandString, DEFAULT, PARAM_USERNAME, 
                    PARAM_SALT, PARAM_HASH, PARAM_FIRST_NAME, 
                    PARAM_LAST_NAME, PARAM_EMAIL, PARAM_ROLE, 
                    DEFAULT);
                var command = new MySqlCommand(commandString);
                command.Parameters.AddWithValue(PARAM_USERNAME, model.Username);
                command.Parameters.AddWithValue(PARAM_SALT, model.PasswordSalt);
                command.Parameters.AddWithValue(PARAM_HASH, model.PasswordHash);
                command.Parameters.AddWithValue(PARAM_FIRST_NAME, model.FirstName);
                command.Parameters.AddWithValue(PARAM_LAST_NAME, model.LastName);
                command.Parameters.AddWithValue(PARAM_EMAIL, model.Email);
                command.Parameters.AddWithValue(PARAM_ROLE, model.Role.RoleID);
                var result = DbConnection.ExecuteCommandNonQuery(command);
                this.ensureSingleRowWasAffected(result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
        private void ensureSingleRowWasAffected(int result)
        {
            if (result != SINGLE_ROW)
            {
                throw new Exception("Could not insert new user");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User Read(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Disables the User specified by the given ID
        /// </summary>
        /// <param name="userID"></param>
        public void Disable(int userID)
        {
            var commandString = "UPDATE user SET {0} = 0 WHERE {1} = {2}";
            commandString = string.Format(commandString, ATTR_ENABLED, ATTR_USER_ID, PARAM_USER_ID);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_USER_ID, userID);
            var numRowsAffected = DbConnection.ExecuteCommandNonQuery(command);
            if (numRowsAffected != 1)
            {
                throw new Exception("Could not disable user. Number of rows affected is not one.");
            }
        }

        /// <summary>
        /// Retrieves user data from the database by the user's unique id.
        /// </summary>
        /// <param name="userID">The User's id</param>
        /// <returns>A User object</returns>
        public string ReadUserFullName(int userID)
        {
            string fullName = "";
            var commandString = "SELECT * FROM user WHERE {0} = {1};";
            commandString = string.Format(commandString, ATTR_USER_ID, PARAM_USER_ID);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_USER_ID, userID);
            using (var reader = DbConnection.ExecuteCommandWithReader(command))
            {
                while(reader.Read())
                {
                    fullName += reader.GetString(ATTR_FIRST_NAME);
                    fullName += " ";
                    fullName += reader.GetString(ATTR_LAST_NAME);
                }
            }
            return fullName;
        }

        /// <summary>
        /// Returns the User whose username is given
        /// </summary>
        /// <param name="usernameToRead"></param>
        /// <returns></returns>
        public User Read(string usernameToRead)
        {
            User user = null;
            var commandString = "SELECT * FROM user WHERE {0} = {1};";
            commandString = string.Format(commandString, ATTR_USERNAME, PARAM_USERNAME);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_USERNAME, usernameToRead);
            using (var reader = DbConnection.ExecuteCommandWithReader(command))
            {
                while (reader.Read())
                {
                    var username = reader.GetString(ATTR_USERNAME);
                    var firstName = reader.GetString(ATTR_FIRST_NAME);
                    var lastName = reader.GetString(ATTR_LAST_NAME);
                    var email = reader.GetString(ATTR_EMAIL);
                    var foreignKeyIsNull = reader.IsDBNull(FK_ROLE_ID_COLUMN);
                    if (foreignKeyIsNull)
                    {
                        user = new User()
                        {
                            Username = username,
                            FirstName = firstName,
                            LastName = lastName,
                            Email = email,
                            Role = new Role()
                            {
                                Name = "None"
                            }
                        };
                    }
                    else
                    {
                        var fkRole = reader.GetInt32(ATTR_ROLE);
                        var role = this.roleRepo.Read(fkRole);
                        user = new User()
                        {
                            UserID = reader.GetInt32(ATTR_USER_ID),
                            Username = username,
                            FirstName = firstName,
                            LastName = lastName,
                            Email = email,
                            Role = role
                        };
                    }
                }
            }
            return user;
        }

        /// <summary>
        /// Returns the Role of the User whose username is given
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Role ReadUserRole(string username)
        {
            var roleID = 0;
            Role role = null;
            try
            {
                var commandString = "SELECT {0} FROM user WHERE {1} = {2};";
                commandString = string.Format(commandString, ATTR_ROLE, ATTR_USERNAME, PARAM_USERNAME);
                var command = new MySqlCommand(commandString);
                command.Parameters.AddWithValue(PARAM_USERNAME, username);
                using (var reader = DbConnection.ExecuteCommandWithReader(command))
                {
                    roleID = getUserRole(roleID, reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("General Exception : " + e);
            }

            role = this.roleRepo.Read(roleID);
            return role;
        }

        private static int getUserRole(int roleID, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                roleID = reader.GetInt32(ATTR_ROLE);
            }
            return roleID;
        }

        /// <summary>
        /// Updates the data of the specified user.
        /// </summary>
        /// <param name="model">The User to update</param>
        public void Update(User user)
        {
            var commandString = "UPDATE user SET {0} = {1}, {2} = {3} WHERE {4} = {5}";
            commandString = string.Format(commandString, ATTR_ROLE, PARAM_ROLE, ATTR_ENABLED, PARAM_ENABLED, ATTR_USER_ID, PARAM_USER_ID);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_ROLE, user.Role.RoleID);
            command.Parameters.AddWithValue(PARAM_ENABLED, Convert.ToInt32(user.Enabled));
            command.Parameters.AddWithValue(PARAM_USER_ID, user.UserID);
            var rowsUpdated = DbConnection.ExecuteCommandNonQuery(command);
            if (rowsUpdated != 1)
            {
                throw new Exception("There was a problem updating this user. The number of rows returned was not equal to 1.");
            }
        }

        /// <summary>
        /// Changes the password fields for a particular user.
        /// </summary>
        /// <param name="username">The User's username</param>
        /// <param name="newPassword">The User's new password</param>
        public void ChangePassword(string username, string newPassword)
        {
            string salt = this.getUserSalt(username);
            string hash = this.hasher.ComputeHash(salt, newPassword);
            var commandString = "UPDATE user SET {0} = {1} WHERE {2} = {3};";
            commandString = string.Format(commandString, ATTR_HASH, PARAM_HASH, ATTR_USERNAME, PARAM_USERNAME);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_HASH, hash);
            command.Parameters.AddWithValue(PARAM_USERNAME, username);
            var result = DbConnection.ExecuteCommandNonQuery(command);
            if (result != 1)
            {
                throw new InvalidOperationException("Could not update password. Found multiple users with username " + username);
            }
        }

        /// <summary>
        /// Deletes the specified user from the database.
        /// </summary>
        /// <param name="model">The User to delete</param>
        public void Destroy(int userID)
        {
            var commandString = "DELETE FROM user WHERE {0} = {1}";
            commandString = string.Format(commandString, ATTR_USER_ID, PARAM_USER_ID);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_USER_ID, userID);
            var rowsDeleted = DbConnection.ExecuteCommandNonQuery(command);
            if (rowsDeleted != 1)
            {
                throw new Exception("There was a problem delelting this role. The number of roles deleted was not exactly one.");
            }
        }

        /// <summary>
        /// Retrieves all user data from the database.
        /// </summary>
        /// <returns>A List of Users</returns>
        public List<User> ReadAll()
        {
            var allUsers = new List<User>();
            try
            {
                var commandString = "SELECT * FROM user;";
                var command = new MySqlCommand(commandString);
                using (var reader = DbConnection.ExecuteCommandWithReader(command)) {
                    while (reader.Read())
                    {
                        var user = new User()
                        {
                            UserID = reader.GetInt32(ATTR_USER_ID),
                            Username = reader.GetString(ATTR_USERNAME),
                            FirstName = reader.GetString(ATTR_FIRST_NAME),
                            LastName = reader.GetString(ATTR_LAST_NAME),
                            Email = reader.GetString(ATTR_EMAIL),
                            Enabled = Convert.ToBoolean(reader.GetInt32(ATTR_ENABLED))
                        };
                        var foreignKeyIsNull = reader.IsDBNull(FK_ROLE_ID_COLUMN);
                        if (!foreignKeyIsNull)
                        {
                            var fkRole = reader.GetInt32(ATTR_ROLE);
                            var role = this.roleRepo.Read(fkRole);
                            //user.Role = role;
                            user.RoleName = role.Name;
                        }
                        else
                        {
                            //user.Role = null;
                            user.RoleName = "None";
                        }
                        allUsers.Add(user);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("General Exception : " + e);
            }
            return allUsers;
        }

        /// <summary>
        /// Get's the specified user's password salt
        /// </summary>
        /// <param name="username"></param>
        /// <returns>The user's password salt</returns>
        public string getUserSalt(string username)
        {
            string salt = "";
            try
            {
                var commandString = "SELECT {0} FROM user WHERE {1} = {2};";
                commandString = string.Format(commandString, ATTR_SALT, ATTR_USERNAME, PARAM_USERNAME);
                var command = new MySqlCommand(commandString);
                command.Parameters.AddWithValue(PARAM_USERNAME, username);
                using (var reader = DbConnection.ExecuteCommandWithReader(command))
                {
                    salt = readSalt(salt, reader);
                }
            }
            catch (MySqlException mse)
            {
                Console.WriteLine("MySqlException : " + mse);
            }
            catch (Exception e)
            {
                Console.WriteLine("General Exception : " + e);
            }
            return salt;
        }

        private string readSalt(string salt, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                salt = reader.GetString(ATTR_SALT);
            }

            return salt;
        }
    }
}
