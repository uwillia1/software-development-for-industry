﻿using ITInventory.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ITInventory.DAL
{
    /// <summary>
    /// The RoleRepository provides an interface 
    /// for interacting with the role 
    /// table in the database.
    /// </summary>
    public class RoleRepository : IRepository<Role>
    {
        /// <summary>
        /// Addes a new Role to the database.
        /// </summary>
        /// <param name="role">The Role to be added</param>
        public void Create(Role role)
        {
            try
            {
                var commandString = "INSERT INTO role(role_id, name) VALUES(DEFAULT, @name)";
                var command = new MySqlCommand(commandString);
                command.Parameters.AddWithValue("@name", role.Name);
                var result = DbConnection.ExecuteCommandNonQuery(command);
                this.ensureSingleRowWasInserted(result);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ensureSingleRowWasInserted(int result)
        {
            if (result != 1)
            {
                throw new Exception("Could not add Role to database");
            }
        }

        /// <summary>
        /// Retrieves the specified role from the database.
        /// </summary>
        /// <param name="id">The id of the desired role</param>
        /// <returns>The specified role</returns>
        public Role Read(int id)
        {
            Role role = null;
            try
            {
                var commandString = "SELECT * FROM role WHERE role_id = @id;";
                var command = new MySqlCommand(commandString);
                command.Parameters.AddWithValue("@id", id);
                using (var reader = DbConnection.ExecuteCommandWithReader(command))
                {
                    role = getSingleRole(role, reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("General Exception : " + e);
            }

            return role;
        }

        private Role getSingleRole(Role role, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                role = new Role()
                {
                    RoleID = reader.GetInt32("role_id"),
                    Name = reader.GetString("name")
                };
            }

            return role;
        }

        /// <summary>
        /// Updates the data of the specified role
        /// </summary>
        /// <param name="roleID">The id of the role to update</param>
        public void Update(Role role)
        {
            var commandString = "UPDATE role SET name = @name WHERE role_id = @roleID";
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue("@name", role.Name);
            command.Parameters.AddWithValue("@roleID", role.RoleID);
            var rowsUpdated = DbConnection.ExecuteCommandNonQuery(command);
            if (rowsUpdated != 1)
            {
                throw new Exception("There was a problem updating this role. The number of rows returned was not equal to 1.");
            }
        }

        /// <summary>
        /// Deletes the specified role from the database.
        /// </summary>
        /// <param name="roleID">The id of the role to delete</param>
        public void Destroy(int roleID)
        {
            var commandString = "DELETE FROM role WHERE role_id = @roleID";
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue("@roleID", roleID);
            var rowsDeleted = DbConnection.ExecuteCommandNonQuery(command);
            if (rowsDeleted != 1)
            {
                throw new Exception("There was a problem delelting this role. The number of roles deleted was not exactly one.");
            }
        }

        /// <summary>
        /// Retrieves all of the role data from the database.
        /// </summary>
        /// <returns>A List of all the Roles</returns>
        public List<Role> ReadAll()
        {
            var allRoles = new List<Role>();
            try
            {
                var commandString = "SELECT * FROM role;";
                var command = new MySqlCommand(commandString);
                using (var reader = DbConnection.ExecuteCommandWithReader(command))
                {
                    getAllRoles(allRoles, reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e);
            }

            return allRoles;
        }

        private static void getAllRoles(List<Role> allRoles, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                var role = new Role()
                {
                    RoleID = reader.GetInt32("role_id"),
                    Name = reader.GetString("name")
                };
                allRoles.Add(role);
            }
        }
    }
}
