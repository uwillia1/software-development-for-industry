﻿using System.Collections.Generic;

namespace ITInventory.DAL
{
    /// <summary>
    /// Defines commonly used methods of repositories
    /// </summary>
    /// <typeparam name="T">A generic place holder for users, items, etc</typeparam>
    public interface IRepository<T>
    {
        void Create(T model);

        T Read(int id);

        void Update(T model);

        void Destroy(int id);

        List<T> ReadAll();
    }
}
