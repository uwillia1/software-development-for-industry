﻿using ITInventory.Model;
using ITInventory.Utility;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ITInventory.DAL
{
    /// <summary>
    /// ItemRepository performs all interactions with the item table in the database.
    /// </summary>
    public class ItemRepository : IRepository<Item>
    {
        private UserRepository userRepo;

        private const string ATTR_ITEM_ID = "item_id";
        private const string ATTR_ITEM_NAME = "name";
        private const string ATTR_SERIAL_NUMBER = "serial_number";
        private const string ATTR_DATE_ADDED = "date_added";
        private const string ATTR_ADDED_BY = "added_by";
        private const string ATTR_LAST_MODIFIED = "last_modified";
        private const string ATTR_MODIFIED_BY = "modified_by";
        private const string ATTR_LOCATION = "location";
        private const string ATTR_PHOTO = "photo";

        private const string PARAM_PREFIX = "@";

        private const string PARAM_ITEM_ID = PARAM_PREFIX + ATTR_ITEM_ID;
        private const string PARAM_ITEM_NAME = PARAM_PREFIX + ATTR_ITEM_NAME;
        private const string PARAM_SERIAL_NUMBER = PARAM_PREFIX + ATTR_SERIAL_NUMBER;
        private const string PARAM_DATE_ADDED = PARAM_PREFIX + ATTR_DATE_ADDED;
        private const string PARAM_ADDED_BY = PARAM_PREFIX + ATTR_ADDED_BY;
        private const string PARAM_LAST_MODIFIED = PARAM_PREFIX + ATTR_LAST_MODIFIED;
        private const string PARAM_MODIFIED_BY = PARAM_PREFIX + ATTR_MODIFIED_BY;
        private const string PARAM_LOCATION = PARAM_PREFIX + ATTR_LOCATION;
        private const string PARAM_PHOTO = PARAM_PREFIX + ATTR_PHOTO;

        /// <summary>
        /// Creates a new instance of the ItemRepository class.
        /// </summary>
        public ItemRepository()
        {
            userRepo = new UserRepository();
        }

        /// <summary>
        /// Adds a new Item to the database.
        /// </summary>
        /// <param name="item"></param>
        public void Create(Item item)
        {
            var commandText = "INSERT INTO item({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}) ";
            commandText = string.Format(commandText, ATTR_ITEM_ID, ATTR_ITEM_NAME, ATTR_SERIAL_NUMBER, ATTR_DATE_ADDED, ATTR_ADDED_BY, ATTR_LAST_MODIFIED, ATTR_MODIFIED_BY, ATTR_LOCATION, ATTR_PHOTO);
            commandText += "VALUES(DEFAULT, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7});";
            commandText = string.Format(commandText, PARAM_ITEM_NAME, PARAM_SERIAL_NUMBER, PARAM_DATE_ADDED, PARAM_ADDED_BY, PARAM_LAST_MODIFIED, PARAM_MODIFIED_BY, PARAM_LOCATION, PARAM_PHOTO);
            var command = new MySqlCommand(commandText);
            command.Parameters.AddWithValue(PARAM_ITEM_NAME, item.Name);
            command.Parameters.AddWithValue(PARAM_SERIAL_NUMBER, item.SerialNumber);
            command.Parameters.AddWithValue(PARAM_DATE_ADDED, item.DateAdded);
            command.Parameters.AddWithValue(PARAM_ADDED_BY, item.AddedByID);
            command.Parameters.AddWithValue(PARAM_LAST_MODIFIED, item.LastModified);
            command.Parameters.AddWithValue(PARAM_MODIFIED_BY, item.ModifiedByID);
            command.Parameters.AddWithValue(PARAM_LOCATION, item.Location);
            command.Parameters.AddWithValue(PARAM_PHOTO, BlobConverter.ToBlob(item.Photo));
            var result = DbConnection.ExecuteCommandNonQuery(command);
            this.ensureSingleRowWasAffected(result);
        }

        private void ensureSingleRowWasAffected(int result)
        {
            if (result != 1)
            {
                throw new Exception("\r\n### The number of rows affected was not exactly 1 ###\r\n");
            }
        }

        /// <summary>
        /// Returns a single Item from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Item Read(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Edits the specified Item in the database.
        /// </summary>
        /// <param name="item"></param>
        public void Update(Item item)
        {
            var commandText = "UPDATE item SET {0} = {1}, {2} = {3}, {4} = {5}, {6} = {7} WHERE {8} = {9};";
            commandText = string.Format(commandText, ATTR_LOCATION, PARAM_LOCATION, ATTR_LAST_MODIFIED, PARAM_LAST_MODIFIED, ATTR_MODIFIED_BY, PARAM_MODIFIED_BY, ATTR_PHOTO, PARAM_PHOTO, ATTR_ITEM_ID, PARAM_ITEM_ID);
            var command = new MySqlCommand(commandText);
            command.Parameters.AddWithValue(PARAM_LOCATION, item.Location);
            command.Parameters.AddWithValue(PARAM_LAST_MODIFIED, item.LastModified);
            command.Parameters.AddWithValue(PARAM_MODIFIED_BY, item.ModifiedByID);
            command.Parameters.AddWithValue(PARAM_PHOTO, BlobConverter.ToBlob(item.Photo));
            command.Parameters.AddWithValue(PARAM_ITEM_ID, item.ItemID);
            var rowsAffected = DbConnection.ExecuteCommandNonQuery(command);
            this.ensureSingleRowWasAffected(rowsAffected);
        }

        /// <summary>
        /// Deletes the specified Item from the database
        /// </summary>
        /// <param name="itemID"></param>
        public void Destroy(int itemID)
        {
            var commandText = "DELETE FROM item WHERE {0} = {1};";
            commandText = string.Format(commandText, ATTR_ITEM_ID, PARAM_ITEM_ID);
            var command = new MySqlCommand(commandText);
            command.Parameters.AddWithValue(PARAM_ITEM_ID, itemID);
            var rowsDeleted = DbConnection.ExecuteCommandNonQuery(command);
            if (rowsDeleted != 1)
            {
                throw new Exception("### Could not delete item. The number of rows affected was not exactly one. ###");
            }
        }

        /// <summary>
        /// Returns all Items from the database.
        /// </summary>
        /// <returns></returns>
        public List<Item> ReadAll()
        {
            var allItems = new List<Item>();
            try
            {
                var commandText = "SELECT * FROM item;";
                var command = new MySqlCommand(commandText);
                using (var reader = DbConnection.ExecuteCommandWithReader(command))
                {
                    this.getAllItems(allItems, reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e);
            }

            return allItems;
        }

        private void getAllItems(List<Item> allItems, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                var item = new Item()
                {
                    ItemID = reader.GetInt32(ATTR_ITEM_ID),
                    Name = reader.GetString(ATTR_ITEM_NAME),
                    SerialNumber = reader.GetString(ATTR_SERIAL_NUMBER),
                    DateAdded = reader.GetString(ATTR_DATE_ADDED).Split(' ')[0],
                    AddedByID = reader.GetInt32(ATTR_ADDED_BY),
                    AddedByUser = this.userRepo.ReadUserFullName(reader.GetInt32(ATTR_ADDED_BY)),
                    LastModified = reader.GetString(ATTR_LAST_MODIFIED).Split(' ')[0],
                    ModifiedByID = reader.GetInt32(ATTR_MODIFIED_BY),
                    ModifiedByUser = this.userRepo.ReadUserFullName(reader.GetInt32(ATTR_MODIFIED_BY)),
                    Location = reader.GetString(ATTR_LOCATION),
                    Photo = BlobConverter.ToImage(reader.GetString(ATTR_PHOTO))
                };
                allItems.Add(item);
            }
        }
    }
}
