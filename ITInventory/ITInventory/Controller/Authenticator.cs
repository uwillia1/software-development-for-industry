﻿using ITInventory.DAL;
using ITInventory.Model;
using MySql.Data.MySqlClient;
using System;

namespace ITInventory.Controller
{
    /// <summary>
    /// Manages user logins
    /// </summary>
    public class Authenticator
    {
        private UserRepository userRepo;
        private PasswordHashser hasher;

        private const string ATTR_USERNAME = "username";
        private const string ATTR_ENABLED = "enabled";
        private const string ATTR_HASH = "password_hash";

        private const string PARAM_PREFIX = "@";

        private const string PARAM_USERNAME = PARAM_PREFIX + ATTR_USERNAME;
        private const string PARAM_ENABLED = PARAM_PREFIX + ATTR_ENABLED;
        private const string PARAM_HASH = PARAM_PREFIX + ATTR_HASH;

        /// <summary>
        /// Makes a new instance of Authenticator
        /// </summary>
        public Authenticator()
        {
            this.userRepo = new UserRepository();
            this.hasher = new PasswordHashser();
        }

        /// <summary>
        /// Logs a user in
        /// </summary>
        /// <param name="username">User's unique username</param>
        /// <param name="password">User's passwoIrd</param>
        /// <returns>True if login successful, false otherwise</returns>
        public bool ValidateLogin(string username, string password)
        {
            bool credentialsAreValid = false;
            var salt = this.userRepo.getUserSalt(username);
            try
            {
                var commandText = "SELECT * FROM user WHERE {0} = {1} AND {2} = {3};";
                commandText = string.Format(commandText, ATTR_USERNAME, PARAM_USERNAME, ATTR_HASH, PARAM_HASH);
                var command = new MySqlCommand(commandText);
                command.Parameters.AddWithValue(PARAM_USERNAME, username);
                string passwordHash = this.hasher.ComputeHash(salt, password);
                command.Parameters.AddWithValue(PARAM_HASH, passwordHash);
                var result = DbConnection.ExecuteCommandWithScalar(command);
                credentialsAreValid = isLoginValid(result);
            }
            catch (MySqlException mse)
            {
                Console.WriteLine("MySqlException : " + mse);
            }
            catch (Exception e)
            {
                Console.WriteLine("General Exception : " + e);
            }
            return credentialsAreValid;
        }

        private bool isLoginValid(object result)
        {
            bool isValid;
            if (result == null)
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }

            return isValid;
        }

        public bool CheckAccountEnabled(string username)
        {
            var enabled = false;
            var commandString = "SELECT {0} FROM user WHERE {1} = {2};";
            commandString = string.Format(commandString, ATTR_ENABLED, ATTR_USERNAME, PARAM_USERNAME);
            var command = new MySqlCommand(commandString);
            command.Parameters.AddWithValue(PARAM_USERNAME, username);
            using (var reader = DbConnection.ExecuteCommandWithReader(command))
            {
                while(reader.Read())
                {
                    enabled = Convert.ToBoolean(reader.GetInt32(ATTR_ENABLED));
                }
            }
            return enabled;
        }
    }
}
