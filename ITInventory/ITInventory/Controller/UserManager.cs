﻿using ITInventory.DAL;
using ITInventory.Model;
using MySql.Data.MySqlClient;
using System;

namespace ITInventory.Controller
{
    /// <summary>
    /// Responsible for CRUD operations for users and roles.
    /// Uses RoleRepository and UserRepository.
    /// </summary>
    public class UserManager
    {
        private UserRepository userRepo;
        private PasswordHashser hasher;
        private RoleRepository roleRepo;

        /// <summary>
        /// Creates a new instance of UserManager.
        /// </summary>
        public UserManager()
        {
            this.userRepo = new UserRepository();
            this.hasher = new PasswordHashser();
            this.roleRepo = new RoleRepository();
        }

        /// <summary>
        /// Adds a new User to the database.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="role"></param>
        public void AddNewUser(string username, string password, string firstName, string lastName, string email, Role role)
        {
            var hasher = new PasswordHashser();
            var salt = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 8);
            var hash = hasher.ComputeHash(salt, password);
            var user = new User()
            {
                Username = username,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Role = role,
                PasswordSalt = salt,
                PasswordHash = hash
            };
            this.userRepo.Create(user);
        }

        internal void updateUser(User user)
        {
            this.userRepo.Update(user);
        }

        /// <summary>
        /// Returns the Role associated with the specified User
        /// </summary>
        /// <param name="username"></param>
        /// <returns>User's role</returns>
        public Role GetUserRole(string username)
        {
            var userRole = this.userRepo.ReadUserRole(username);
            return userRole;
        }

        internal void deleteUser(int userID)
        {
            this.userRepo.Destroy(userID);
        }

        private int getSingleRole(int roleID, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                roleID = (int)reader.GetInt64("fk_user_role_role_id");
            }
            return roleID;
        }

        private string getUserRoleName(int roleID)
        {
            return this.roleRepo.Read(roleID).Name;
        }

        private string getSingleRoleName(string roleName, MySqlDataReader reader)
        {
            while (reader.Read())
            {
                roleName = reader.GetString("name");
            }
            return roleName;
        }

        /// <summary>
        /// Changes a user's password to the one specified.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="newPassword"></param>
        public void ChangePassword(string username, string newPassword)
        {
            this.userRepo.ChangePassword(username, newPassword);
        }

        /// <summary>
        /// Deletes a role from the database
        /// </summary>
        /// <param name="roleID"></param>
        public void deleteRole(int roleID)
        {
            this.roleRepo.Destroy(roleID);
        }

        /// <summary>
        /// Changes a Role's information in the database
        /// </summary>
        /// <param name="role"></param>
        public void updateRole(Role role)
        {
            this.roleRepo.Update(role);
        }

        /// <summary>
        /// Returns a user from the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public User readUser(string username)
        {
            return this.userRepo.Read(username);
        }

        /// <summary>
        /// Disables a User, meaning their account still 
        /// exists, but cannot be logged into or used.
        /// </summary>
        /// <param name="userID"></param>
        public void DisableUser(int userID)
        {
            this.userRepo.Disable(userID);
        }
    }
}

